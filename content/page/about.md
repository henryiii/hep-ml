---
title: About this site
subtitle: Why hep-ml exists
comments: false
---

This site is intended to be a place for curated content on Machine Learning in High Energy Physics.
